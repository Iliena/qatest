﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
namespace SearchGoogle_v2
{
    public partial class Form1 : Form
    {
        RemoteWebDriver driver = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void btFirefox_Click(object sender, EventArgs e)
        {
            if (driver != null)
                driver.Close();
            if (tbText.Text == "")
                return;

            driver = new FirefoxDriver();
            GetPage();
        }

        private void btChrome_Click(object sender, EventArgs e)
        {
            if (driver != null)
                driver.Close();
            if (tbText.Text == "")
                return;
            driver = new ChromeDriver();
            GetPage();
        }

        private void btIE_Click(object sender, EventArgs e)
        {
            if (driver != null)
                driver.Close();
            if (tbText.Text == "")
                return;
            driver = new InternetExplorerDriver();
            GetPage();
        }

        private void GetPage()
        {
            if (driver == null) return;
            try
            {
                driver.Navigate().GoToUrl("http://www.google.com");
                IWebElement search = driver.FindElementByXPath("//input[@title='Поиск']");
                search.SendKeys(tbText.Text);

                //IWebElement submit = driver.FindElementByXPath("//button[@type='submit']");
                //submit.Click();
                WaitForElement();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Произошла известная ошибка, которую еще не устранили :(" + Environment.NewLine +ex.Message);
            }

        }

        private void WaitForElement()
        {
            IWebElement element = new WebDriverWait(this.driver, TimeSpan.FromSeconds(3)).Until(ExpectedConditions.ElementExists(By.XPath("//ul[@role='listbox']")));
            var children = element.FindElements(By.XPath("descendant::div[@class='sbqs_c']"));
            children[2].Click();
           
        }
    }
}
